/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * DFL Login1 class implements a part of the systemd logind dbus protocol.
 **/

#include <fcntl.h>
#include <unistd.h>
#include <QtDBus>

#include "Login1.hpp"
#include "Login1Impl.hpp"

DFL::Login1::Login1( QObject *parent ) : QObject( parent ) {
    impl = new Login1Impl();

    /** Connect to the login1 interface  */
    impl->login1 = new QDBusInterface(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        QDBusConnection::systemBus()
    );

    /** Connect to the login1's session interface  */
    impl->session1 = new QDBusInterface(
        "org.freedesktop.login1",
        "/org/freedesktop/login1/session/self",
        "org.freedesktop.login1.Session",
        QDBusConnection::systemBus()
    );

    QDBusConnection::systemBus().connect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1/session/self",
        "org.freedesktop.login1.Session",
        "Lock",
        this,
        SLOT( LockSession() )
    );

    QDBusConnection::systemBus().connect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1/session/self",
        "org.freedesktop.login1.Session",
        "Unlock",
        this,
        SLOT( UnlockSession() )
    );

    QString orgName( qApp->organizationName() );
    QString appName( qApp->applicationName() );

    if ( orgName.isEmpty() or appName.isEmpty() ) {
        qCritical() << "Unable to start the DBus-based interal service.";
        return;
    }

    impl->appName = appName;
}


DFL::Login1::~Login1() {
    releaseInhibitLocks();

    delete impl->login1;
    delete impl->session1;
}


void DFL::Login1::acquireInhibitLocks( DFL::Login1::InhibitorLocks locks ) {
    if ( locks & DFL::Login1::SleepLock ) {
        QDBusReply<QDBusUnixFileDescriptor> reply = impl->login1->call(
            "Inhibit",
            "sleep",
            impl->appName,
            "Handled by the DE.",
            "block"
        );

        if ( reply.isValid() ) {
            impl->sleepLockFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
        }
    }

    if ( locks & DFL::Login1::ShutdownLock ) {
        QDBusReply<QDBusUnixFileDescriptor> reply = impl->login1->call(
            "Inhibit",
            "shutdown",
            impl->appName,
            "Handled by the DE.",
            "block"
        );

        if ( reply.isValid() ) {
            impl->shutdownLockFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
        }
    }

    if ( locks & DFL::Login1::IdleLock ) {
        QDBusReply<QDBusUnixFileDescriptor> reply = impl->login1->call(
            "Inhibit",
            "idle",
            impl->appName,
            "Handled by the DE.",
            "block"
        );

        if ( reply.isValid() ) {
            impl->idleLockFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
        }
    }

    QString keyLockNames;

    keyLockNames += (locks & DFL::Login1::PowerKeyLock ? "handle-power-key" : "");
    keyLockNames += (locks & DFL::Login1::SuspendKeyLock ? "handle-suspend-key" : "");
    keyLockNames += (locks & DFL::Login1::HibernateKeyLock ? "handle-hibernate-key" : "");

    if ( keyLockNames.count() ) {
        QDBusReply<QDBusUnixFileDescriptor> reply = impl->login1->call(
            "Inhibit",
            keyLockNames,
            impl->appName,
            "Handled by the DE.",
            "block"
        );

        if ( reply.isValid() ) {
            impl->keyLocksFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
        }
    }

    if ( locks & DFL::Login1::LidSwitchLock ) {
        QDBusReply<QDBusUnixFileDescriptor> reply = impl->login1->call(
            "Inhibit",
            "handle-lid-switch",
            impl->appName,
            "Handled by the DE.",
            "block"
        );

        if ( reply.isValid() ) {
            impl->lidSwitchLockFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
        }
    }

    QDBusConnection::systemBus().connect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        "PrepareForSleep",
        this,
        SLOT( PrepareForSleep( bool ) )
    );

    QDBusConnection::systemBus().connect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        "PrepareForShutdown",
        this,
        SLOT( PrepareForShutdown( bool ) )
    );
}


void DFL::Login1::releaseInhibitLocks() {
    close( impl->sleepLockFD );
    close( impl->shutdownLockFD );
    close( impl->idleLockFD );
    close( impl->keyLocksFD );
    close( impl->lidSwitchLockFD );

    QDBusConnection::systemBus().disconnect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        "PrepareForSleep",
        this,
        SLOT( PrepareForSleep( bool ) )
    );

    QDBusConnection::systemBus().disconnect(
        "org.freedesktop.login1",
        "/org/freedesktop/login1",
        "org.freedesktop.login1.Manager",
        "PrepareForShutdown",
        this,
        SLOT( PrepareForShutdown( bool ) )
    );
}


bool DFL::Login1::setBrightness( double brightness ) {
    QDirIterator dirIt( "/sys/class/backlight", QDir::Dirs | QDir::NoDotAndDotDot );

    int failed = 0;

    while ( dirIt.hasNext() ) {
        /** Get the next entry */
        dirIt.next();

        /** Get the info corresponding to that entry  */
        QFileInfo info( dirIt.fileInfo() );

        /** Read the maximum brightness */
        QFile max( dirIt.filePath() + "/max_brightness" );
        max.open( QFile::ReadOnly );
        int maxVal = max.readAll().toInt();
        max.close();

        if ( maxVal == 0 ) {
            maxVal = 100;
        }

        QDBusReply<void> reply = impl->session1->call( "SetBrightness", "backlight", dirIt.fileName(), ( uint )(brightness * maxVal / 100.0) );

        if ( not reply.isValid() ) {
            qWarning() << "Error changing brightness of " + dirIt.fileName() + " :" << reply.error().message();
            failed++;
        }
    }

    return (failed ? false : true);
}


bool DFL::Login1::setBrightness( QString driver, QString device, uint brightness ) {
    QDBusReply<void> reply = impl->session1->call( "SetBrightness", driver, device, brightness );

    if ( not reply.isValid() ) {
        qWarning() << "Error changing brightness:" << reply.error().message();
        return false;
    }

    return true;
}


int DFL::Login1::request( QString request ) {
    if ( request.startsWith( "Can" ) ) {
        QDBusReply<QString> reply = impl->login1->call( request );

        if ( not reply.isValid() ) {
            return -3;
        }

        if ( reply.value() == "na" ) {
            return -2;
        }

        else if ( reply.value() == "challenge" ) {
            return -1;
        }

        else if ( reply.value() == "no" ) {
            return 0;
        }

        else {
            return 1;
        }
    }

    else {
        /** We need to release the correct lock. */
        if ( (request == "PowerOff") or (request == "Reboot") ) {
            close( impl->shutdownLockFD );
            impl->shutdownLockFD = -1;
        }

        else {
            close( impl->sleepLockFD );
            impl->sleepLockFD = -1;
        }

        /**
         * Authorization may be needed to perform this operation.
         * Or others may have taken delay/block locks.
         * The DE has taken care of inhibit locks (hopefully).
         * So we can force the locks, or allow PKit to perform
         * authorization.
         */
        QDBusReply<void> reply = impl->login1->call( request, true );

        if ( not reply.isValid() ) {
            return 0;
        }

        else {
            return 1;
        }
    }
}


int DFL::Login1::scheduleShutdown( QString type, qulonglong usec ) {
    if ( (type != "poweroff") and (type != "reboot") and (type != "cancel") ) {
        return 0;
    }

    if ( type == "cancel" ) {
        /** Cancel the scheduled shutdown */
        QDBusReply<bool> cancelReply = impl->login1->call( "CancelScheduledShutdown" );

        /** Re-acquire inhibit lock */
        QDBusReply<QDBusUnixFileDescriptor> lockReply = impl->login1->call(
            "Inhibit",
            "shutdown",
            impl->appName,
            "Handled by the DE.",
            "block"
        );

        if ( lockReply.isValid() ) {
            impl->shutdownLockFD = fcntl( lockReply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
        }

        if ( cancelReply.isValid() ) {
            /**
             * Return 0 on failed, and 1 on cancellation
             */
            return cancelReply.value();
        }

        else {
            return 0;
        }
    }

    else {
        /** Close the inhibit locks */
        close( impl->shutdownLockFD );
        impl->shutdownLockFD = -1;

        /** Perform the dbus call to schedule the request */
        QDBusReply<void> reply = impl->login1->call( "ScheduleShutdown", type, usec );

        if ( not reply.isValid() ) {
            return 0;
        }

        else {
            return 1;
        }
    }
}


void DFL::Login1::PrepareForSleep( bool active ) {
    /** Someone requested Sleep; such requests are ignored */
    if ( active ) {
        //
    }

    /** We're coming out of Sleep (or failed); re-acquire the lock */
    else {
        if ( impl->sleepLockFD == -1 ) {
            QDBusReply<QDBusUnixFileDescriptor> reply;
            reply = impl->login1->call( "Inhibit", "sleep", impl->appName, "Handled by the DE.", "block" );

            if ( reply.isValid() ) {
                impl->sleepLockFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
            }
        }
    }
}


void DFL::Login1::PrepareForShutdown( bool active ) {
    /** Someone requested Shutdown; such requests are ignored */
    if ( active ) {
        //
    }

    /** Shutdown failed; re-acquire the lock */
    else {
        if ( impl->shutdownLockFD == -1 ) {
            QDBusReply<QDBusUnixFileDescriptor> reply;
            reply = impl->login1->call( "Inhibit", "shutdown", impl->appName, "Handled by the DE.", "block" );

            if ( reply.isValid() ) {
                impl->shutdownLockFD = fcntl( reply.value().fileDescriptor(), F_DUPFD_CLOEXEC, 3 );
            }
        }
    }
}


void DFL::Login1::LockSession() {
    qDebug() << "ScreenLockRequested";
}


void DFL::Login1::UnlockSession() {
    qDebug() << "ScreenUnlockRequested";
}
