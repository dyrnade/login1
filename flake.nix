{
  description = "A flake for building Hello World";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-unstable;

  outputs = { self, nixpkgs }: {

    packages.x86_64-linux.default =
      # Notice the reference to nixpkgs here.
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation rec {
  pname = "login1";
  version = "v0.1.1";
  # https://gitlab.com/desktop-frameworks/ipc.git
  src = fetchFromGitLab {
    hash = "sha256-iPujkSKXHXd60EG7L+s87/ejTotPbRHKXwrLdFDzzpU=";
    domain = "gitlab.com";
    owner = "desktop-frameworks";
    repo = pname;
    rev = version;
  };
  outputs = [ "out" ];
  #mesonAutoFeatures = "auto";
  nativeBuildInputs = [
    ninja meson pkgconfig cmake python3 qt6.wrapQtAppsHook
  ];
  buildInputs = [
    qt6.qtbase
  ];
  mesonFlags = [ "--prefix=${placeholder "out"}/usr --buildtype=release -Duse_qt_version=qt6" ];
      };

  };
}

